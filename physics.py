#!/usr/bin/env python
#
# Code Philippe  Di Stefano, Queen's
# A basic class for sources and detectors - has dimensions
#

import math
import random
#import matplotlib.pyplot as plt

import geometry as ge

verbose= False

# Absorption properties
class matter(object):
	def __init__(self, attLen_cm=1.0):
		# The attenuation length
		self.attLen_cm=attLen_cm
	
	# The probability that a particle travelling a given distance escapes
	def pEsc(self, d_cm):
		return math.exp(-d_cm/self.attLen_cm)
		
	# A draw if a particle travelling a given distance escapes
	def esc(self, d_cm):
		return True if random.uniform(0,1) < self.pEsc(d_cm) else False
	
	# A draw if the particle is absorbed
	def abs(self, d_cm):
		return (not self.esc(d_cm))

# A source, with self-absorption		
class source(ge.prismSource, matter):
	def __init__(self, xMinMax_cm=[-1.0, 1.0], yMinMax_cm=[-1.0, 1.0], zMinMax_cm=[-1.0, 1.0], n=10, attLen_cm=1.0):
		ge.prismSource.__init__(self, xMinMax=xMinMax_cm, yMinMax=yMinMax_cm, zMinMax=zMinMax_cm, n=n)
		matter.__init__(self, attLen_cm=attLen_cm)
		
		# The probability that each ray escapes
		self.pEscape=[self.pEsc(d_cm) for d_cm in self.distInSource]
		
		# A random draw if each ray escapes
		self.escape = [self.esc(d_cm) for d_cm in self.distInSource]
		
		if verbose:
			print "Dist in source (cm)\tProb escape\tEscaped?"
			for i in range(0,10):
				print str(self.distInSource[i]) +"\t" + str(self.pEscape[i]) +"\t" + str(self.escape[i])
		
# A detector, with absorption
class detector(ge.rectPrism, matter):
	def __init__(self, xMinMax_cm=[-1.0, 1.0], yMinMax_cm=[-1.0, 1.0], zMinMax_cm=[-1.0, 1.0], attLen_cm=1.0):
		ge.rectPrism.__init__(self, xMinMax=xMinMax_cm, yMinMax=yMinMax_cm, zMinMax=zMinMax_cm)
		matter.__init__(self, attLen_cm=attLen_cm)
		
		
	def detectRay(self, ray):
		d_cm= ray.distanceInPrism(self)
		#return self.abs(d_cm)
		return {"Detected": self.abs(d_cm), "Distance (cm)": d_cm}

# A routine for a source and a detector
def simConfig(src, det):
	emitted=0
	escapeSource=0
	reachDetector=0
	detected=0
	for i in range(0, len(src.srcs)):
		emitted += 1
		if src.escape[i]:
			escapeSource += 1
			res=det.detectRay(src.srcs[i])
			if res["Distance (cm)"] > 0.0:
				reachDetector +=1
			if res["Detected"]:
				detected += 1
	if verbose:
		print "Emitted in source: " + str(emitted) + "\tEscaped from source: " + str(escapeSource) + "\tReached detector: " + str(reachDetector) + "\tDetected: " + str(detected)		
	return {"Emmited in source": emitted, "Escaped from source": escapeSource, "Reached detector": reachDetector, "Detected": detected}

if __name__ == '__main__':
	n=1000
	
	print "Basic source and detector"
	src=source(n=n)
	det=detector()
	print "\t" + str(simConfig(src, det))
	