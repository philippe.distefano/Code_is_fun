#!/usr/bin/env python
#
# Code Philippe  Di Stefano, Queen's
# Code to test physics.py and geometry.py
#

import math
import random
import matplotlib.pyplot as plt
import numpy as np


import physics as ph


n=100000

def simSize():

	print "Detector around source"
	src=ph.source(n=n)
	det=ph.detector()
	print "\t"+ str(ph.simConfig(src, det))

	
if __name__ == '__main__':
	simSize()
