#!/usr/bin/env python
#
# Code Philippe  Di Stefano, Queen's
# A basic class for simple geometric elements - no dimensions
#

import math
import random
import matplotlib.pyplot as plt

verbose= False
showFigs = True
writeFigs = True
outPath = "/Users/distefano/Documents/Code_Svn/PHYS_313/TestOut/"

class LineInPlaneError(Exception): pass

class NoIntersectionError(Exception): pass

### A simple class for 3D points, equivalent to 3D vectors
class point(object):						
	def __init__(self, coords=[0.0, 0.0, 0.0]):
		# Test that there are exactly 3 coords
		if len(coords) !=3:
			print "Wrong number of coordinates"
			raise ValueError("Wrong number of coordinates")
		self.x=coords[0]
		self.y=coords[1]
		self.z=coords[2]
		self.coords=[self.x, self.y, self.z]
	
	def __str__(self):
		return "Point/Vector: " + str(self.coords)
		
	def isZero(self):
		if (self.x==0.0) and (self.y==0.0) and (self.z==0.0):
			return True
		return False

	def equals(self, pt):
		if self.x==pt.x and self.y==pt.y and self.z==pt.z:
			return True
		return False
		
### Scalar product between two vectors
def scalProd(v1=point(), v2=point()):
	return v1.x*v2.x + v1.y*v2.y + v1.z*v2.z

### Difference between two points
def difference(x1=point(), x2=point()):
	return point(coords=[x1.x-x2.x, x1.y-x2.y, x1.z-x2.z])
	
### Distance between two points
def distance(x1=point(), x2=point()):
	dif=difference(x1,x2)
	prod=scalProd(dif, dif)
	return math.sqrt(prod)
	
### A class for 3D lines, defined as M+tV
### x=m1+tv1
### y=m2+tv2
### z=m3+tv3
class line(object):
	def __init__(self, pt=point(), vec=point()):
		self.pt= pt
		self.vec= vec
		self.name="Line"
	
	def __str__(self):
		return self.name + " -> Pt: " + str(self.pt) + ", Vec: " + str(self.vec)
	
	# Gets a point on the line, defined by parameter
	def getPoint(self, t):
		x=self.pt.x+t*self.vec.x
		y=self.pt.y+t*self.vec.y
		z=self.pt.z+t*self.vec.z
		return point([x,y,z])

	#  Only used for daughter class (ray)
	def test(self, t):
		return True
		
	# There can be no intersection, or the line can be in the plane, or there can be a single intersection
	def intersecPlane(self, pln):
		p1=scalProd(self.vec,pln.norm)
		p2=pln.d-scalProd(self.pt,pln.norm)
		
		#print "0"
		
		if p1==0:
			if p2==0:
				##print str(self.name) +" in plane"
				#print "1"
				raise LineInPlaneError
			else:
				##print "No intersection"
				#print "2"
				raise NoIntersectionError
		else:
			t=p2/p1
			if verbose:
				print t
			if self.test(t):
				pt=self.getPoint(t)
				if pln.testInBounds(pt):
					if verbose:
						print "Intersection: " + str(pt)
					return pt
				else:
					##print "No intersection"
					#print "3"
					raise NoIntersectionError
			else:
				##print "No intersection"
				#print "4"
				raise NoIntersectionError
				
	def intersecPrism(self, prism):
		intPts=[]
		for pl in prism.planes:
			try:
				pt=self.intersecPlane(pl)
				newPt=True
				# A point can appear several times in the list,
				# eg if the intersection is at an appex or a line.
				for ppt in intPts:
					if pt.equals(ppt):
						newPt=False
				if newPt:
					intPts.append(pt)
			except LineInPlaneError:
				#print LineInPlaneError
				pass
			except NoIntersectionError:
				#print NoIntersectionError
				pass
		if verbose:
			print "Intersection between\n\t"+ str(self)+"\n\tand prism"
			for pt in intPts:
				print "\t\t" + str(pt)

		if len(intPts) >2:
			print "Weird intersection"
		return intPts
		
	def distanceInPrism(self, prism):
		dist=0.0
		intPts=self.intersecPrism(prism)
		if len(intPts)==2:
			if verbose:
				print "2 pt intersection"
			dist=distance(intPts[0], intPts[1])
		elif len(intPts)==1 and self.name=="Ray":
			if verbose:
				print "1 pt intersection of ray"
			dist=distance(intPts[0], self.pt)
		return dist

### The part of a line that starts at the point and goes in the direction of the vector (ie positive parameters only)				
class ray(line):		
	def __init__(self, pt=point(), vec=point()):
		line.__init__(self, pt=pt, vec=vec)
		self.name="Ray"

	def test(self, t):
		if t<0.0:
			return False
		return True


### Plane defined by normal vector N and constant d: n1x+n2y+n3z=d
### Line defined by x=m1+tv1, y=m2+tv2, z=m3+tv3
### Intersection: n1m1+n2m2+n3m3+t(n1v1+n2v2+n3v3)=d -> t(n1v1+n2v2+n3v3)=d-n1m1-n2m2-n3m3
class plane(object):
	def __init__(self, norm=point(), d=0.0):
	 	# Test that the normal is not zero
	 	if norm.isZero():
	 		print norm
	 		print "Zero normal alert!"
			raise ValueError("Zero normal alert!")
		self.norm=norm
		self.d=d	
		if verbose:
			print self.norm, self.d
		self.name="Plane"
	
	def __str__(self):
		return self.name + " -> Norm: " +str(self.norm) + ", d: " + str(self.d)

	#  Only used for daughter class (rectangle)
	def testInBounds(self, pt):
		return True

# A plane in the XY direction
class xyRect(plane):
	def __init__(self, z=0.0, xMinMax=[-1.0, 1.0], yMinMax=[-1.0, 1.0]):
		plane.__init__(self, norm=point([0.0, 0.0, 1.0]), d=z)
		self.xMinMax=xMinMax
		self.yMinMax=yMinMax
		self.name="xyRect"

	def __str__(self):
		return self.name + " -> z: " +str(self.d) + ", x: " + str(self.xMinMax) + ", y: " + str(self.yMinMax)
	
	def testInBounds(self, pt):
		if (pt.x < self.xMinMax[0]) or (pt.x > self.xMinMax[1]) or (pt.y < self.yMinMax[0]) or (pt.y > self.yMinMax[1]):
			return False
		return True

# A plane in the YZ direction		
class yzRect(plane):
	def __init__(self, x=0.0, yMinMax=[-1.0, 1.0], zMinMax=[-1.0, 1.0]):
		plane.__init__(self, norm=point([1.0, 0.0, 0.0]), d=x)
		self.yMinMax=yMinMax
		self.zMinMax=zMinMax
		self.name="yzRect"
	
	def __str__(self):
		return self.name + " -> x: " +str(self.d) + ", y " + str(self.yMinMax) + ", z: " + str(self.zMinMax)

	def testInBounds(self, pt):
		if (pt.y < self.yMinMax[0]) or (pt.y > self.yMinMax[1]) or (pt.z < self.zMinMax[0]) or (pt.z > self.zMinMax[1]):
			return False
		return True
		
# A plane in the ZX direction		
class zxRect(plane):
	def __init__(self, y=0.0, zMinMax=[-1.0, 1.0], xMinMax=[-1.0, 1.0]):
		plane.__init__(self, norm=point([0.0, 1.0, 0.0]), d=y)
		self.zMinMax=zMinMax
		self.xMinMax=xMinMax
		self.name="zxRect"
	
	def __str__(self):
		return self.name + " -> y: " +str(self.d) + ", z " + str(self.zMinMax) + ", x: " + str(self.xMinMax)

	def testInBounds(self, pt):
		if (pt.z < self.zMinMax[0]) or (pt.z > self.zMinMax[1]) or (pt.x < self.xMinMax[0]) or (pt.x > self.xMinMax[1]):
			return False
		return True

# A rectangular prism made from the previous planes
class rectPrism(object):
	def __init__(self, xMinMax=[-1.0, 1.0], yMinMax=[-1.0, 1.0], zMinMax=[-1.0, 1.0]):
		self.xMinMax=xMinMax
		self.yMinMax=yMinMax
		self.zMinMax=zMinMax
		
		self.xMin=yzRect(x=xMinMax[0], yMinMax=yMinMax, zMinMax=zMinMax)
		self.xMax=yzRect(x=xMinMax[1], yMinMax=yMinMax, zMinMax=zMinMax)
		self.yMin=zxRect(y=yMinMax[0], zMinMax=zMinMax, xMinMax=xMinMax)
		self.yMax=zxRect(y=yMinMax[1], zMinMax=zMinMax, xMinMax=xMinMax)
		self.zMin=xyRect(z=zMinMax[0], xMinMax=xMinMax, yMinMax=yMinMax)
		self.zMax=xyRect(z=zMinMax[1], xMinMax=xMinMax, yMinMax=yMinMax)
		
		# Note that a line can't have the same intersection with both x planes since they don't touch
		self.planes=[self.xMin, self.xMax, self.yMin, self.yMax, self.zMin, self.zMax]

# A ray whose direction is drawn randomly from a uniform distribution
class isoRay(ray):
	def __init__(self, pt=point()):
		ray.__init__(self, pt=pt, vec=point())
		self.vec.theta=random.uniform(0.0, 2.0*math.pi) # Azimuth angle
		if True:	# Correct method 
			self.vec.z=random.uniform(-1.0, 1.0)	# Cos of zenith angle
			self.vec.phi=math.acos(self.vec.z)	# Zenith angle
		else:	# Illustrate incorrect method
			self.vec.phi=random.uniform(0.0, math.pi)
			self.vec.z=math.cos(self.vec.phi)
		self.vec.x=math.sin(self.vec.phi)*math.cos(self.vec.theta)
		self.vec.y=math.sin(self.vec.phi)*math.sin(self.vec.theta)
		self.vec.coords=[self.vec.x, self.vec.y, self.vec.z]

# A prism filled with uniformly distributed sources
class prismSource(rectPrism):
	def __init__(self, xMinMax=[-1.0, 1.0], yMinMax=[-1.0, 1.0], zMinMax=[-1.0, 1.0], n=10):
		rectPrism.__init__(self, xMinMax=xMinMax, yMinMax=yMinMax, zMinMax=zMinMax)
		self.srcs=[isoRay(pt=point([random.uniform(xMinMax[0], xMinMax[1]), random.uniform(yMinMax[0], yMinMax[1]), random.uniform(zMinMax[0], zMinMax[1])])) for i in range(0,n)]
		self.distInSource=[src.distanceInPrism(self) for src in self.srcs]
		
		self.x=[src.pt.x for src in self.srcs]
		self.y=[src.pt.y for src in self.srcs]
		self.z=[src.pt.z for src in self.srcs]


#### All the rest is test routines

def testPrismSource():
	n=100000
	bns=40
	
	ps =prismSource(n=n)
	
	f, tmpArr = plt.subplots(2,2)
	ax = tmpArr.ravel()

	f.suptitle('Rectangular prism (' + str(n) + " draws)", fontsize=14)
	ax[0].hist2d(ps.x,ps.y,bins=[bns, bns])
	ax[1].hist2d(ps.y,ps.z,bins=[bns, bns])
	ax[2].hist2d(ps.z,ps.x,bins=[bns, bns])
	for axx, lab in zip(ax,[["x","y"], ["y", "z"], ["z", "x"]]):
		axx.set_xlabel(lab[0] + ' Position')
		axx.set_ylabel(lab[1] + ' Position')
		
	ax[3].hist(ps.distInSource, bins=bns,  histtype='step', log=True)
	ax[3].set_yscale('log')
	ax[3].set_xlabel('Distance in Source')
	ax[3].set_ylabel('Counts')

	f.text(1.0, 0.02, " " + str("geometry.py"), fontsize=10, color='gray', ha='center', va='center', alpha=0.2, rotation=0, horizontalalignment='right', verticalalignment='center')

	if writeFigs:	
		plt.savefig(outPath + "PrismTest.png")
	if showFigs:
		plt.show()

def testIsoRay():
	#plt.clf()
	n=100000
	bns=40
	pts=[isoRay() for i in range(0,n)]
	
	f, tmpArr = plt.subplots(2,2)
	ax = tmpArr.ravel()
	x=[iso.vec.x for iso in pts]
	y=[iso.vec.y for iso in pts]
	z=[iso.vec.z for iso in pts]
	if False:
		ax[0].hist(x,bins=bns)
		ax[1].hist(y,bins=bns)
		ax[2].hist(z,bins=bns)
	else:
		ax[0].hist2d(x,y,bins=[bns, bns])
		ax[1].hist2d(y,z,bins=[bns, bns])
		ax[2].hist2d(z,x,bins=[bns, bns])

	f.text(1.0, 0.02, " " + str("geometry.py"), fontsize=10, color='gray', ha='center', va='center', alpha=0.2, rotation=0, horizontalalignment='right', verticalalignment='center')
		
	if writeFigs:	
		plt.savefig(outPath + "PrismTest.png")
	if showFigs:
		plt.show()

def testIntersec(line, plane):
	print str(line) + "\n\tintersects " + str(plane) + "\n\tat ",
	try:
		print str(line.intersecPlane(plane))
	except LineInPlaneError:
		print LineInPlaneError
	except NoIntersectionError:
		print NoIntersectionError
		
def testDistance(x1=point(), x2=point()):
	print "Distance between " + str(x1) + " and " + str(x2) + ": " + str(distance(x1, x2))
		
if __name__ == '__main__':
	if False:
		x1=point([1.0, 1.0, 1.0])
		x2=point([-1.0, -1.0, -1.0])
		x3=point([-1.0, 0.0, 0.0])
		testDistance(x1=x1,x2=x2)
		testDistance(x1=x1)
		testDistance(x1=x3)

	if False:
		P1=plane(norm=point([1.0,0.0,0.0]), d=-1.0)
		L1=line(pt=point([0.0, 0.0, 0.0]), vec=point([1.0,0.0,0.0]))
		print str(L1) + "\n\tintersects " + str(P1) + "\n\tat " + str(L1.intersecPlane(P1)) # Should be [-1.0, 0.0, 0.0]

		R1=ray(pt=point([0.0, 0.0, 0.0]), vec=point([1.0,0.0,0.0]))
		print str(R1) + "\n\tintersects " + str(P1) + "\n\tat " + str(R1.intersecPlane(P1)) # Should be None

		P2=plane(norm=point([1.0,0.0,0.0]), d=1.0)
		print str(R1) + "\n\tintersects " + str(P2) + "\n\tat " + str(R1.intersecPlane(P2)) # Should be [1.0, 0.0, 0.0]

		P3=plane(norm=point([0.0,1.0,0.0]), d=0.0)
		print str(L1) + "\n\tintersects " + str(P3) + "\n\tat " + str(L1.intersecPlane(P3)) # Should be Line in plane

		P3=plane(norm=point([0.0,1.0,0.0]), d=0.0)
		print str(R1) + "\n\tintersects " + str(P3) + "\n\tat " + str(R1.intersecPlane(P3)) # Should be Ray in plane
		
	if False:
		xy1=xyRect()
		xy2=xyRect(z=-1.0)
		yz1=yzRect()
		yz2=yzRect(x=-4)
		yz3=yzRect(x=-4,yMinMax=[2.0, 4.0])
		xl=line(pt=point([0.0, 0.0, 0.0]), vec=point([1.0,0.0,0.0]))
		testIntersec(xl, xy1)
		testIntersec(xl, xy2)
		testIntersec(xl, yz1)
		testIntersec(xl, yz2)
		testIntersec(xl, yz3)
		#point(coords=[0.0, 0.0, 0.0])
		#point(coords=[0.0, 0.0])
		
	if False:
		xl1=line(pt=point([0.0, 0.0, 0.0]), vec=point([1.0,0.0,0.0]))
		xl2=ray(pt=point([0.0, 0.0, 0.0]), vec=point([1.0,0.0,0.0]))
		xl3=line(pt=point([0.0, 0.0, 1.0]), vec=point([1.0,0.0,0.0]))
		xl4=line(pt=point([0.0, 0.0, 2.0]), vec=point([1.0,0.0,0.0]))
		xl5=ray(pt=point([2.0, 0.0, 0.0]), vec=point([1.0,0.0,0.0]))
		xl5=ray(pt=point([2.0, 0.0, 0.0]), vec=point([1.0,0.0,0.0]))
		xl6=line(pt=point([0.0, 0.0, 0.0]), vec=point([10.0,10.0,10.0]))	# An example with duplicate intersections
		xl7=line(pt=point([0.0, 0.0, 0.0]), vec=point([-10.0,10.0,-10.0]))	# An example with duplicate intersections
		xl8=line(pt=point([0.0, 0.0, 0.0]), vec=point([-10.0,10.0,0.0]))	# An example with duplicate intersections
		#print str(xl)
		rp = rectPrism()
		xl1.intersecPrism(rp)
		print "\t\t\t -> " + str(xl1.distanceInPrism(rp))+"\n"
		xl2.intersecPrism(rp)
		print "\t\t\t -> " + str(xl2.distanceInPrism(rp))+"\n"
		xl3.intersecPrism(rp)
		print "\t\t\t -> " + str(xl3.distanceInPrism(rp))+"\n"
		xl4.intersecPrism(rp)
		print "\t\t\t -> " + str(xl4.distanceInPrism(rp))+"\n"
		xl5.intersecPrism(rp)
		print "\t\t\t -> " + str(xl5.distanceInPrism(rp))+"\n"
		xl6.intersecPrism(rp)
		print "\t\t\t -> " + str(xl6.distanceInPrism(rp))+"\n"
		xl7.intersecPrism(rp)
		print "\t\t\t -> " + str(xl7.distanceInPrism(rp))+"\n"
		xl8.intersecPrism(rp)
		print "\t\t\t -> " + str(xl8.distanceInPrism(rp))+"\n"
		
	if True:
		#testIsoRay()
		#ps=prismSource()
		testPrismSource()
